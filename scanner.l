%{
/* #include "nodeInfo.h" */

#include "y.tab.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


char strbuf[301];

/* nodeInfo tmp; */

%}

%x STATE_STRING
%x STATE_COMMENT

%%

" "+ {}
\t+ {}
\n {}

"for" {yylval.str = strdup(yytext); return FOR;}
"do" {yylval.str = strdup(yytext); return DO;}
"while" {yylval.str = strdup(yytext); return WHILE;}
"break" {yylval.str = strdup(yytext);return BREAK;}
"continue" {yylval.str = strdup(yytext); return CONTINUE;}
"if" {yylval.str = strdup(yytext); return IF;}
"else" {yylval.str = strdup(yytext); return ELSE;}
"return" {yylval.str = strdup(yytext); return RETURN;}
"switch" {yylval.str = strdup(yytext); return SWITCH;}
"case" {yylval.str = strdup(yytext); return CASE;}
"default" {yylval.str = strdup(yytext); return DEFAULT;}
"void" {yylval.str = strdup(yytext); return VOID;}
"int" {yylval.str = strdup(yytext);return INT;}
"double" {yylval.str = strdup(yytext); return DOUBLE;}
"float" {yylval.str = strdup(yytext); return FLOAT;}
"char" {yylval.str = strdup(yytext); return CHAR;}
"const" {yylval.str = strdup(yytext);return CONST;}
"signed" {yylval.str = strdup(yytext); return SIGNED;}
"unsigned" {yylval.str = strdup(yytext); return UNSIGNED;}
"short" {yylval.str = strdup(yytext); return SHORT;}
"long" {yylval.str = strdup(yytext); return LONG;}

"NULL" {yylval.intVal = 0; return LITERAL_NUM;}
"HIGH" {yylval.intVal = 1; return LITERAL_NUM;}
"LOW" {yylval.intVal = 0; return LITERAL_NUM;}

[_[:alpha:]][_[:alnum:]]* {yylval.str = strdup(yytext); return IDENTIFIER;}

"+" {yylval.cVal = yytext[0]; return '+';}
"-" {yylval.cVal = yytext[0]; return '-';}
"*" {yylval.cVal = yytext[0]; return '*';}
"/" {yylval.cVal = yytext[0]; return '/';}
"%" {yylval.cVal = yytext[0]; return '%';}
"++" {yylval.str = strdup(yytext); return OP_INC;}
"--" {yylval.str = strdup(yytext); return OP_DEC;}
"<" {yylval.cVal = yytext[0]; return '<';}
"<=" {yylval.str = strdup(yytext); return OP_LE;}
">" {yylval.cVal = yytext[0]; return '>';}
">=" {yylval.str = strdup(yytext); return OP_GE;}
"==" {yylval.str = strdup(yytext); return OP_EQ;}
"!=" {yylval.str = strdup(yytext); return OP_NE;}
"=" {yylval.cVal = yytext[0]; return '=';}
"&&" {yylval.str = strdup(yytext); return OP_LOGICAL_AND;}
"||" {yylval.str = strdup(yytext); return OP_LOGICAL_OR;}
"!" {yylval.cVal = yytext[0]; return '!';}
"~" {yylval.cVal = yytext[0]; return '~';}
"^" {yylval.cVal = yytext[0]; return '^';}
"&" {yylval.cVal = yytext[0]; return '&';}
"|" {yylval.cVal = yytext[0]; return '|';}

"<<" {yylval.str = strdup(yytext); return OP_SHIFT_L;}
">>" {yylval.str = strdup(yytext); return OP_SHIFT_R;}

":" {yylval.cVal = yytext[0]; return ':';}
";" {yylval.cVal = yytext[0]; return ';';}
"," {yylval.cVal = yytext[0]; return ',';}
"." {yylval.cVal = yytext[0]; return '.';}
"[" {yylval.cVal = yytext[0]; return '[';}
"]" {yylval.cVal = yytext[0]; return ']';}
"(" {yylval.cVal = yytext[0]; return '(';}
")" {yylval.cVal = yytext[0]; return ')';}
"{" {yylval.cVal = yytext[0]; return '{';}
"}" {yylval.cVal = yytext[0]; return '}';}

[[:digit:]]+ {yylval.intVal = atoi(yytext); return LITERAL_NUM;}

(([0-9]*[.][0-9]+)|([0-9]+[.][0-9]*)) {yylval.dVal = atof(yytext); return LITERAL_DOUBLE;}



\" {BEGIN STATE_STRING; strcpy(strbuf, "\"");} 
<STATE_STRING>\\[abefnrtv0] {strcat(strbuf, yytext);}
<STATE_STRING>\\\\ {strcat(strbuf, yytext);}
<STATE_STRING>\\' {strcat(strbuf, yytext);}
<STATE_STRING>\\\" {strcat(strbuf, yytext);}
<STATE_STRING>\\\? {strcat(strbuf, yytext);}
<STATE_STRING>\\[0-7]{1,3} {strcat(strbuf, yytext);}
<STATE_STRING>\\x[0-9A-Fa-f]+ {strcat(strbuf, yytext);}
<STATE_STRING>\\u[0-9A-Fa-f]{4,4} {strcat(strbuf, yytext);}
<STATE_STRING>\\U[0-9A-Fa-f]{8,8} {strcat(strbuf, yytext);}

<STATE_STRING>\" {BEGIN(INITIAL); strcat(strbuf, yytext); yylval.str = strdup(strbuf); return LITERAL_STRING;}

<STATE_STRING>. {strcat(strbuf, yytext);}


'\\[abefnrtv0]' {yylval.str = strdup(yytext); return LITERAL_CHAR;}
'\\\\' {yylval.str = strdup(yytext); return LITERAL_CHAR;}
'\\'' {yylval.str = strdup(yytext); return LITERAL_CHAR;}
'\\\"' {yylval.str = strdup(yytext); return LITERAL_CHAR;}
'\\\?' {yylval.str = strdup(yytext); return LITERAL_CHAR;}
'\\[0-7]{1,3}' {yylval.str = strdup(yytext); return LITERAL_CHAR;}
'\\x[0-9A-Fa-f]+' {yylval.str = strdup(yytext); return LITERAL_CHAR;}
'\\u[0-9A-Fa-f]{4,4}' {yylval.str = strdup(yytext); return LITERAL_CHAR;}
'\\U[0-9A-Fa-f]{8,8}' {yylval.str = strdup(yytext); return LITERAL_CHAR;}
'.' {yylval.str = strdup(yytext); return LITERAL_CHAR;}



\/\/.*\n {}

"/*" {BEGIN STATE_COMMENT;}
<STATE_COMMENT>. {}
<STATE_COMMENT>\n {}
<STATE_COMMENT>"*/" {BEGIN(INITIAL);}


%%

// eliminate warning of unused variables warning...
int (*input_funcPointer)(void) = input;
void (*yyunput_funcPointer)(int, char*) = yyunput;
