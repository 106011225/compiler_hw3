#ifndef __CODE_H__
#define __CODE_H__


#define MAX_TABLE_SIZE 5000

#define MAXNUM_LOCAL_VAR 8
#define NUM_CALLEE_SAVE_REG 13
#define NUM_CALLER_SAVE_REG 16

void init_symbol_table();
int install_symbol(char *s, int variant);
int look_up_symbol(char *s);
void pop_up_symbol(int scope);
void set_scope_and_offset_of_param(char *s);
void set_local_vars(char *functor);
void set_global_vars(char *s);
void code_gen_func_header(char *functor);
// void code_gen_global_vars();
void code_gen_at_end_of_function_body(char *functor);
void code_gen_caller_before(int numArg);
void code_gen_caller_after(int numArg);
char* copys(char* s);

typedef struct symbol_entry *PTR_SYMB;
struct symbol_entry {
   char *name;
   int scope;
   int offset;
   int id;
   int variant;
   int type;
   int total_args;
   int total_locals;
   int mode;
   int functor_index; /* add for risc-v, 2020 by Jenq-Kuen Lee */
}  table[MAX_TABLE_SIZE];


#define T_FUNCTION 1
#define ARGUMENT_MODE   2
#define LOCAL_MODE      4
#define GLOBAL_MODE     8

// variant
#define V_VAR_NAME 1
#define V_FUNC_NAME 2

// type
#define TP_VOID 1
#define TP_INT 2
#define TP_POINTER 3

extern int cur_scope;
extern int cur_counter;


#endif /* __CODE_H__ */
