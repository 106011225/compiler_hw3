
for f in *.c; do
    NAME_ANS=${f/.c/.ans};
    NAME_OUT=${f/.c/.out};
    golden_parser < $f > $NAME_ANS;
    ../parser < $f > $NAME_OUT;
    diff $NAME_ANS $NAME_OUT -s;
done

rm *.ans *.out
