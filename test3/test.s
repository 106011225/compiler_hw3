.global codegen
codegen:
    addi sp, sp, -104 
    sd sp, 96(sp) 
    sd fp, 88(sp) 
    sd s1, 80(sp) 
    sd s2, 72(sp) 
    sd s3, 64(sp) 
    sd s4, 56(sp) 
    sd s5, 48(sp) 
    sd s6, 40(sp) 
    sd s7, 32(sp) 
    sd s8, 24(sp) 
    sd s9, 16(sp) 
    sd s10, 8(sp) 
    sd s11, 0(sp) 
    addi fp, sp, 104 
    
    li t0, 26 
    addi sp, sp, -8 
    sd t0, 0(sp) 
     
    li t0, 1 
    addi sp, sp, -8 
    sd t0, 0(sp) 
     
    ld a1, 0(sp) 
    ld a0, 8(sp) 
    addi sp, sp, 16 
    addi sp, sp, -8 
    sd ra, 0(sp) 
    jal ra, digitalWrite 
    ld ra, 0(sp) 
    addi sp, sp, 8 
     
    li t0, 1000 
    addi sp, sp, -8 
    sd t0, 0(sp) 
     
    ld a0, 0(sp) 
    addi sp, sp, 8 
    addi sp, sp, -8 
    sd ra, 0(sp) 
    jal ra, delay 
    ld ra, 0(sp) 
    addi sp, sp, 8 
     
    li t0, 26 
    addi sp, sp, -8 
    sd t0, 0(sp) 
     
    li t0, 0 
    addi sp, sp, -8 
    sd t0, 0(sp) 
     
    ld a1, 0(sp) 
    ld a0, 8(sp) 
    addi sp, sp, 16 
    addi sp, sp, -8 
    sd ra, 0(sp) 
    jal ra, digitalWrite 
    ld ra, 0(sp) 
    addi sp, sp, 8 
     
    li t0, 1000 
    addi sp, sp, -8 
    sd t0, 0(sp) 
     
    ld a0, 0(sp) 
    addi sp, sp, 8 
    addi sp, sp, -8 
    sd ra, 0(sp) 
    jal ra, delay 
    ld ra, 0(sp) 
    addi sp, sp, 8 
     
    ld sp, 96(sp) 
    ld fp, 88(sp) 
    ld s1, 80(sp) 
    ld s2, 72(sp) 
    ld s3, 64(sp) 
    ld s4, 56(sp) 
    ld s5, 48(sp) 
    ld s6, 40(sp) 
    ld s7, 32(sp) 
    ld s8, 24(sp) 
    ld s9, 16(sp) 
    ld s10, 8(sp) 
    ld s11, 0(sp) 
    addi sp, sp, 104 
    jalr zero, 0(ra) 
