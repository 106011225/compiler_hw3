# Compiler assignments: Code generation



## Goal

- Given a C source code, generate 64-bit RISC-V assembly code.
    - At least, a function `void codegen();` would be present in the input C source code.
        - This is the entry point of the generated program.
- The generated assembly will be test on RISC-V Spike simulator:
    1. Replace content of `codegen.S` with the generated assembly code.
    2. Compile the program with `$ riscv64-unknown-elf-gcc main.c codegen.S`.
    3. Execute the program with `$ spike pk a.out`.


## Usage

1. Generate the source code of scanner and parser
    ```
    $ flex scanner.l
    $ byacc -d -v parser.y
    ```

2. Compile the source code of `codegen`
    ```
    $ gcc lex.yy.c y.tab.c code.c -o codegen -lfl
    ```

3. Input a C file to get the RISC-v assembly code
    ```
    $ ./codegen < test.c
    ```

4. Compile the code to get the RISC-V executable file
    ```
    $ riscv64-unknown-elf-gcc main.c codegen.S
    ```

5. Test the result with RISC-V Spike simulator
    ```
    $ spike pk a.out
    ```


**Please refer to [SPEC.pdf](./SPEC.pdf) for more details.**
