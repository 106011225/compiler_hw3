%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* #include "nodeInfo.h" */
#include "code.h"


// add this to avoid warning: implicit declaration of function ‘yylex’ [-Wimplicit-function-declaration]
int yylex(void);

void code_gen_with_header(char *filename);
void install_Arduino_func(void);

FILE *f_asm;

/* nodeInfo ndArr[20]; */
char buf[301];
int idx;
int numLocalVar;

/* nodeInfo ch2nd(char c); */
/* nodeInfo concatChild(int numChild, char* tagType); */
%}

%union{
    int intVal;
    double dVal;
    char cVal;
    char* str;
}


%start start

%token<intVal> LITERAL_NUM 
%token<dVal> LITERAL_DOUBLE 
%token<cVal> ';' ':' ',' '=' '*' '[' ']' '{' '}' '(' ')' '|' '^' '&' '<' '>' '+' '-' '/' '%' '!' '~' 
%token<str> IDENTIFIER LITERAL_CHAR LITERAL_STRING
%token<str> CONST SIGNED UNSIGNED LONG SHORT INT CHAR FLOAT DOUBLE VOID 
%token<str> OP_EQ OP_NE OP_LE OP_GE OP_SHIFT_L OP_SHIFT_R OP_LOGICAL_OR OP_LOGICAL_AND OP_INC OP_DEC
%token<str> BREAK CONTINUE RETURN IF ELSE SWITCH CASE DEFAULT WHILE DO FOR 

%type<str> start global_item_list global_item scalar_decl array_decl func_decl func_def
%type<str> type ident_list type_specifier_list type_specifier iden_initORnot scalar_pointerORnot 
%type<str> array_list array_initORnot arr_size arr_content arr_content_list 
%type<str> parameter_list parameter stmt_or_vardecl_list 
%type<str> expr_assignment_list expr_assignment expr_logical_or expr_logical_and 
%type<str> expr_bitwise_or expr_bitwise_xor expr_bitwise_and expr_equality expr_relational expr_shift 
%type<str> expr_additive expr_multiplicative expr_cast expr_unary expr_postfix expr_primary variable literal 
%type<str> stmt if_stmt switch_stmt while_stmt for_stmt return_stmt compound_stmt switch_clause_list switch_clause 


%left ','
%right '='
%left OP_LOGICAL_OR
%left OP_LOGICAL_AND
%left '|'
%left '^' 
%left '&'
%left OP_EQ OP_NE
%left OP_LE OP_GE '<' '>'
%left OP_SHIFT_L OP_SHIFT_R
%left '+' '-'
%left '*' '/' '%'
%right ADDRESS_OF 
%right DEREFERENCE 
%right'!' '~'
%right UPLUS UMINUS 
%right OP_INC OP_DEC // prefix
%left SUFFIX_INC SUFFIX_DEC // suffix 
%left '[' ']'
%left '(' ')'



%%


////////////////////////////////// begin global

start 
    : global_item_list {}
    /* : global_item_list {} */
    ;

global_item_list
    : global_item_list global_item {}
    | global_item {}
    ;

global_item
    : scalar_decl {}
    | array_decl {}
    | func_decl {}
    | func_def {}
    ;

///////////////////////////////// end global begin scalar

scalar_decl
    : type ident_list ';' {}
    ;

type
    : CONST type_specifier_list {}
    | type_specifier_list  {}
    | CONST  {}
    ;

type_specifier_list
    : type_specifier_list type_specifier {}
    | type_specifier {} 
    ;

type_specifier
    : SIGNED {}
    | UNSIGNED {}
    | LONG {}
    | SHORT {}
    | INT  {}
    | CHAR {}
    | FLOAT {}
    | DOUBLE {}
    | VOID {}
    ;

ident_list
    : ident_list ',' iden_initORnot {}
    | iden_initORnot  {}
    ;

iden_initORnot
    : scalar_pointerORnot 
            {
                buf = $1;
                if (buf[0] = '*'){ // pointer
                    idx = install_symbol(&(buf[1]), V_VAR_NAME);
                    table[idx].offset = ++numLocalVar;
                    table[idx].type = TP_POINTER;
                    //ASSERT(numLocalVar <= MAXNUM_LOCAL_VAR)
                }
                else { // normal var
                    idx = install_symbol($1, V_VAR_NAME);
                    table[idx].offset = ++numLocalVar;
                    //ASSERT(numLocalVar <= MAXNUM_LOCAL_VAR)
                }
            }
    | scalar_pointerORnot '=' expr_assignment_list 
            {
                buf = $1;
                if (buf[0] = '*'){ // pointer
                    idx = install_symbol(&(buf[1]), V_VAR_NAME);
                    table[idx].offset = ++numLocalVar;
                    table[idx].type = TP_POINTER;
                    //ASSERT(numLocalVar <= MAXNUM_LOCAL_VAR)

                    fprintf(f_asm, "    ld t0, 0(sp) \n"); 
                    fprintf(f_asm, "    sd t0, %d(fp) \n", -8*(NUM_CALLEE_SAVE_REG + table[idx].offset));
                    fprintf(f_asm, "    addi sp, sp, 8 \n");
                    fprintf(f_asm, "    \n");
                }
                else { // normal var
                    idx = install_symbol($1, V_VAR_NAME);
                    table[idx].offset = (++numLocalVar) - 1;
                    //ASSERT(numLocalVar <= MAXNUM_LOCAL_VAR)

                    fprintf(f_asm, "    ld t0, 0(sp) \n"); 
                    fprintf(f_asm, "    sd t0, %d(fp) \n", -8*(NUM_CALLEE_SAVE_REG + table[idx].offset));
                    fprintf(f_asm, "    addi sp, sp, 8 \n");
                    fprintf(f_asm, "    \n");
                }
            }
    ;

scalar_pointerORnot
    : IDENTIFIER {}
    | '*' IDENTIFIER %prec DEREFERENCE {buf[0] = '\0'; strcat(buf, $1); strcat(buf, $2); $$ = strdup(buf);}
    ;


/////////////////////////// end scalar begin array_decl
    
array_decl
    : type array_list ';' {}
    ;

array_list
    : array_list ',' array_initORnot {}
    | array_initORnot {}
    ;
    
array_initORnot
    : IDENTIFIER arr_size {}
    | IDENTIFIER arr_size '=' arr_content {}
    ;

arr_size
    : arr_size '[' expr_assignment_list ']' {}
    | '[' expr_assignment_list ']' {}
    ;

arr_content
    : '{' arr_content_list '}' {}
    | '{' expr_assignment_list '}' {}
    ;

arr_content_list
    : arr_content_list ',' arr_content {}
    | arr_content {}
    ;

/////////////////////////// end array_decl begin func_decl 

func_decl
    : type scalar_pointerORnot '(' ')' ';'  
            {// '*' not handle...
                idx = install_symbol($2, V_FUNC_NAME); 
                table[idx].total_args = 0;
            } 
    | type scalar_pointerORnot '(' parameter_list ')' ';' {}
    ;

parameter_list
    : parameter_list ',' parameter {}
    | parameter {}
    ;

parameter
    : type scalar_pointerORnot  {}
    ;

/////////////////////////// end func_decl begin func_def

func_def
    : type scalar_pointerORnot '(' ')' 
            {
                cur_scope++;
                set_scope_and_offset_of_param($2);
                code_gen_func_header($2);

                numLocalVar = 0;
            }
      '{' stmt_or_vardecl_list 
            {
                pop_up_symbol(cur_scope);
                cur_scope--;
                code_gen_at_end_of_function_body($2);
            }
      '}' 
                
    | type scalar_pointerORnot '(' parameter_list ')' '{' stmt_or_vardecl_list '}' {}
    ;

/////////////////////////// end func_def begin expr 


// precedence rank 15 (precedence of operator)
expr_assignment_list 
    : expr_assignment_list ',' expr_assignment {}
    | expr_assignment {}
    ;

// precedence rank 14
expr_assignment
    : expr_unary '=' expr_assignment {}
    | expr_logical_or {}
    ;

// precedence rank 12 (13 is not used)
expr_logical_or
    : expr_logical_or OP_LOGICAL_OR expr_logical_and  {}
    | expr_logical_and {}
    ;

// precedence rank 11
expr_logical_and
    : expr_logical_and OP_LOGICAL_AND expr_bitwise_or {}
    | expr_bitwise_or {}
    ;

// precedence rank 10
expr_bitwise_or
    : expr_bitwise_or '|' expr_bitwise_xor {}
    | expr_bitwise_xor {}
    ;

// precedence rank 9
expr_bitwise_xor
    : expr_bitwise_xor '^' expr_bitwise_and {}
    | expr_bitwise_and {}
    ;

// precedence rank 8
expr_bitwise_and
    : expr_bitwise_and '&' expr_equality {}
    | expr_equality {}
    ;

// precedence rank 7
expr_equality
    : expr_equality OP_EQ expr_relational {}
    | expr_equality OP_NE expr_relational {}
    | expr_relational {}
    ;

// precedence rank 6
expr_relational
    : expr_relational '<' expr_shift {}
    | expr_relational OP_LE expr_shift {}
    | expr_relational '>' expr_shift {}
    | expr_relational OP_GE expr_shift {}
    | expr_shift {}
    ;

// precedence rank 5
expr_shift
    : expr_shift OP_SHIFT_L expr_additive {}
    | expr_shift OP_SHIFT_R expr_additive {}
    | expr_additive {}
    ;

// precedence rank 4
expr_additive
    : expr_additive '+' expr_multiplicative 
            {
                fprintf(f_asm, "    ld t1, 0(sp) \n");
                fprintf(f_asm, "    addi sp, sp, 8 \n");
                fprintf(f_asm, "    ld t0, 0(sp) \n");
                fprintf(f_asm, "    addi sp, sp, 8 \n");
                fprintf(f_asm, "    add t0, t0, t1 \n");
                fprintf(f_asm, "    addi sp, sp, -8 \n");
                fprintf(f_asm, "    sd t0, 0(sp) \n");
                fprintf(f_asm, "    \n");
            }
    | expr_additive '-' expr_multiplicative 
            {
                fprintf(f_asm, "    ld t1, 0(sp) \n");
                fprintf(f_asm, "    addi sp, sp, 8 \n");
                fprintf(f_asm, "    ld t0, 0(sp) \n");
                fprintf(f_asm, "    addi sp, sp, 8 \n");
                fprintf(f_asm, "    sub t0, t0, t1 \n");
                fprintf(f_asm, "    addi sp, sp, -8 \n");
                fprintf(f_asm, "    sd t0, 0(sp) \n");
                fprintf(f_asm, "    \n");
            }
    | expr_multiplicative {}
    ;

// precedence rank 3
expr_multiplicative
    : expr_multiplicative '*' expr_cast 
            {
                fprintf(f_asm, "    ld t1, 0(sp) \n");
                fprintf(f_asm, "    addi sp, sp, 8 \n");
                fprintf(f_asm, "    ld t0, 0(sp) \n");
                fprintf(f_asm, "    addi sp, sp, 8 \n");
                fprintf(f_asm, "    mul t0, t0, t1 \n");
                fprintf(f_asm, "    addi sp, sp, -8 \n");
                fprintf(f_asm, "    sd t0, 0(sp) \n");
                fprintf(f_asm, "    \n");
            }
    | expr_multiplicative '/' expr_cast 
            {
                fprintf(f_asm, "    ld t1, 0(sp) \n");
                fprintf(f_asm, "    addi sp, sp, 8 \n");
                fprintf(f_asm, "    ld t0, 0(sp) \n");
                fprintf(f_asm, "    addi sp, sp, 8 \n");
                fprintf(f_asm, "    div t0, t0, t1 \n");
                fprintf(f_asm, "    addi sp, sp, -8 \n");
                fprintf(f_asm, "    sd t0, 0(sp) \n");
                fprintf(f_asm, "    \n");
            }
    | expr_multiplicative '%' expr_cast 
            {
                fprintf(f_asm, "    ld t1, 0(sp) \n");
                fprintf(f_asm, "    addi sp, sp, 8 \n");
                fprintf(f_asm, "    ld t0, 0(sp) \n");
                fprintf(f_asm, "    addi sp, sp, 8 \n");
                fprintf(f_asm, "    rem t0, t0, t1 \n");
                fprintf(f_asm, "    addi sp, sp, -8 \n");
                fprintf(f_asm, "    sd t0, 0(sp) \n");
                fprintf(f_asm, "    \n");
            }
    | expr_cast {}
    ;

// precedence rank 2 (The operand of prefix ++ and -- can't be a type cast)
expr_cast
    : '(' type ')' expr_cast {}
    | '(' type '*' ')' expr_cast {}
    | expr_unary {}
    ;

// precedence rank 2
expr_unary
    : '&' expr_cast %prec ADDRESS_OF 
            {
                idx = look_up_symbol($2);
                fprintf(f_asm, "    addi t0, fp, %d \n", -8*(NUM_CALLEE_SAVE_REG + table[idx].offset)); 
                fprintf(f_asm, "    addi sp, sp, -8 \n");
                fprintf(f_asm, "    sd t0, 0(sp) \n");
                fprintf(f_asm, "    \n");
            }
    | '*' expr_cast %prec DEREFERENCE 
            {
                idx = look_up_symbol($2);

                fprintf(f_asm, "    ld t0, %d(fp) \n", -8*(NUM_CALLEE_SAVE_REG + table[idx].offset));
                fprintf(f_asm, "    ld t1, 0(t0) \n");
                fprintf(f_asm, "    addi sp, sp, -8 \n");
                fprintf(f_asm, "    sd t1, 0(sp) \n");
                fprintf(f_asm, "    \n");
            }
    | '!' expr_cast {}
    | '~' expr_cast {}
    | '+' expr_cast %prec UPLUS {}
    | '-' expr_cast %prec UMINUS {}
    | OP_INC expr_unary {}
    | OP_DEC expr_unary {}
    | expr_postfix {}
    ;

// precedence rank 1
expr_postfix
    : expr_postfix '(' ')' {}
    | expr_postfix '(' expr_assignment_list ')' 
            {
                idx = look_up_symbol($1);
                int numArg = table[idx].total_args;
                code_gen_caller_before(numArg);
                fprintf(f_asm, "    jal ra, %s \n", $1);

                // handle return value
                if (table[idx].type != TP_VOID){
                    
                }

                code_gen_caller_after(numArg);
            }
//    | expr_postfix '[' expr_assignment_list ']' {}
    | expr_postfix OP_INC %prec SUFFIX_INC {}
    | expr_postfix OP_DEC %prec SUFFIX_DEC {}
    | expr_primary {}
    ;

// parentheses dictate a new precedence squence for the enclosed "expr"
expr_primary
    : variable {}
    | literal {}
    | '(' expr_assignment_list ')' {}
    ;

variable
    : IDENTIFIER  
            {
                // not completed, only fine for the same scope
                idx = look_up_symbol($1);
                fprintf(f_asm, "    //%s %d, %d\n", $1, idx, table[idx].variant);
                if (table[idx].variant == V_VAR_NAME) {
                    fprintf(f_asm, "    ld t0, %d(fp) \n", -8*(NUM_CALLEE_SAVE_REG + table[idx].offset));
                    fprintf(f_asm, "    addi sp, sp, -8 \n");
                    fprintf(f_asm, "    sd t0, 0(sp) \n");
                    fprintf(f_asm, "    \n");
                }
                
            }
    | IDENTIFIER arr_size  {}
    ;

literal
    : LITERAL_NUM 
            {
                fprintf(f_asm, "    li t0, %d \n", $1);
                fprintf(f_asm, "    addi sp, sp, -8 \n");
                fprintf(f_asm, "    sd t0, 0(sp) \n");
                fprintf(f_asm, "     \n");
            }
    | LITERAL_DOUBLE {}
    | LITERAL_CHAR  {}
    | LITERAL_STRING  {}
    ;

/////////////////////////// end expr begin stmt 
    
stmt
    : expr_assignment_list ';'  {}
    | if_stmt  {}
    | switch_stmt  {}
    | while_stmt  {}
    | for_stmt  {}
    | return_stmt  {}
    | BREAK ';'  {}
    | CONTINUE ';'  {}
    | compound_stmt  {}
    ;

if_stmt
    : IF '(' expr_assignment_list ')' '{' '}' {}
    | IF '(' expr_assignment_list ')' '{' stmt_or_vardecl_list '}' {}
    | IF '(' expr_assignment_list ')' '{' '}' ELSE '{' '}' {}
    | IF '(' expr_assignment_list ')' '{' stmt_or_vardecl_list '}' ELSE '{' '}' {}
    | IF '(' expr_assignment_list ')' '{' '}' ELSE '{' stmt_or_vardecl_list '}' {}
    | IF '(' expr_assignment_list ')' '{' stmt_or_vardecl_list '}' ELSE '{' stmt_or_vardecl_list '}' {}
    ;


switch_stmt
    : SWITCH '(' expr_assignment_list ')' '{' switch_clause_list '}' {}
    | SWITCH '(' expr_assignment_list ')' '{' '}' {}
    ;

switch_clause_list
    : switch_clause_list switch_clause {}
    | switch_clause {}
    ;

switch_clause
    : CASE expr_assignment_list ':' stmt_or_vardecl_list {}
    | DEFAULT ':' stmt_or_vardecl_list {}
    ;


while_stmt
    : WHILE '(' expr_assignment_list ')' stmt {}
    | DO stmt WHILE '(' expr_assignment_list ')' ';' {}
    ;

for_stmt
    : FOR '(' expr_assignment_list ';' expr_assignment_list ';' expr_assignment_list ')' stmt {}
    | FOR '(' ';' expr_assignment_list ';' expr_assignment_list ')' stmt {}
    | FOR '(' expr_assignment_list ';'  ';' expr_assignment_list ')' stmt {}
    | FOR '(' expr_assignment_list ';' expr_assignment_list ';'  ')' stmt {}
    | FOR '(' ';'  ';' expr_assignment_list ')' stmt {}
    | FOR '(' ';' expr_assignment_list ';' ')' stmt {}
    | FOR '(' expr_assignment_list ';' ';' ')' stmt {}
    | FOR '(' ';' ';' ')' stmt {}
    ;

return_stmt
    : RETURN expr_assignment_list ';'  {}
    | RETURN ';' {}
    ;

compound_stmt
    : '{' '}' {}
    | '{' stmt_or_vardecl_list '}' {}
    ;

stmt_or_vardecl_list
    : stmt_or_vardecl_list stmt {}
    | stmt_or_vardecl_list scalar_decl {}
    | stmt_or_vardecl_list array_decl {}
    | stmt {}
    | scalar_decl {}
    | array_decl {}
    ;


///////////////////////// end stmt




%%

int main(void){

    // open file for output asm
	if ((f_asm = fopen("codegen.S", "w")) == NULL) {
		fprintf(stderr, "Cannot open the file %s for write\n", "codegen.S");
		exit(1);
	}

    init_symbol_table();
    install_Arduino_func();
    /* code_gen_with_header() */
    yyparse();

    fclose(f_asm);

    return 0;
}

void install_Arduino_func(void){
    int idx;
    idx = install_symbol("digitalWrite", V_FUNC_NAME); 
    table[idx].total_args = 2;
    table[idx].type = TP_VOID;

    idx = install_symbol("delay", V_FUNC_NAME); 
    table[idx].total_args = 1;
    table[idx].type = TP_VOID;
}

void code_gen_with_header(char *filename) {
	fprintf(f_asm,"      .text\n");
	fprintf(f_asm,"      .file \"%s\"\n",filename);
    fprintf(f_asm, "     \n");
}


void yyerror(char* msg){
    fprintf(stderr, "%s\n", msg);
    exit(1);
}



/* nodeInfo ch2nd(char c){ */
/*     char str[2]; */
/*     sprintf(str, "%c", c); */

/*     nodeInfo retVal; */
/*     retVal.len = 1; */
/*     retVal.str = strdup(str); */
    
/*     return retVal; */
/* } */

/* nodeInfo concatChild(int numChild, char* tagType){ */
/*     int i;  */
/*     nodeInfo retVal; */
/*     retVal.len = 0; */

    
/*     for (i = 0; i < numChild; i++)  */
/*         retVal.len += ndArr[i].len;         */

/*     if (tagType == NULL){ */
/*         retVal.str = (char*)malloc((retVal.len+1)*sizeof(char)); */
/*         *(retVal.str) = '\0'; */
/*         for (i = 0; i < numChild; i++) { */
/*             strcat(retVal.str, ndArr[i].str); */
/*             free(ndArr[i].str); */
/*         } */
/*     } */
/*     else if (!strcmp(tagType, "sdc")){ */
/*         retVal.len += 27; // tag length */
/*         retVal.str = (char*)malloc((retVal.len+1)*sizeof(char)); */
/*         *(retVal.str) = '\0'; */
/*         strcat(retVal.str, "<scalar_decl>"); */
/*         for (i = 0; i < numChild; i++) { */
/*             strcat(retVal.str, ndArr[i].str); */
/*             free(ndArr[i].str); */
/*         } */
/*         strcat(retVal.str, "</scalar_decl>"); */
/*     } */
/*     else if (!strcmp(tagType, "adc")){ */
/*         retVal.len += 25; // tag length */
/*         retVal.str = (char*)malloc((retVal.len+1)*sizeof(char)); */
/*         *(retVal.str) = '\0'; */
/*         strcat(retVal.str, "<array_decl>"); */
/*         for (i = 0; i < numChild; i++) { */
/*             strcat(retVal.str, ndArr[i].str); */
/*             free(ndArr[i].str); */
/*         } */
/*         strcat(retVal.str, "</array_decl>"); */
/*     } */
/*     else if (!strcmp(tagType, "fdc")){ */
/*         retVal.len += 23; // tag length */
/*         retVal.str = (char*)malloc((retVal.len+1)*sizeof(char)); */
/*         *(retVal.str) = '\0'; */
/*         strcat(retVal.str, "<func_decl>"); */
/*         for (i = 0; i < numChild; i++) { */
/*             strcat(retVal.str, ndArr[i].str); */
/*             free(ndArr[i].str); */
/*         } */
/*         strcat(retVal.str, "</func_decl>"); */
/*     } */
/*     else if (!strcmp(tagType, "fdf")){ */
/*         retVal.len += 21; // tag length */
/*         retVal.str = (char*)malloc((retVal.len+1)*sizeof(char)); */
/*         *(retVal.str) = '\0'; */
/*         strcat(retVal.str, "<func_def>"); */
/*         for (i = 0; i < numChild; i++) { */
/*             strcat(retVal.str, ndArr[i].str); */
/*             free(ndArr[i].str); */
/*         } */
/*         strcat(retVal.str, "</func_def>"); */
/*     } */
/*     else if (!strcmp(tagType, "expr")){ */
/*         retVal.len += 13; // tag length */
/*         retVal.str = (char*)malloc((retVal.len+1)*sizeof(char)); */
/*         *(retVal.str) = '\0'; */
/*         strcat(retVal.str, "<expr>"); */
/*         for (i = 0; i < numChild; i++) { */
/*             strcat(retVal.str, ndArr[i].str); */
/*             free(ndArr[i].str); */
/*         } */
/*         strcat(retVal.str, "</expr>"); */
/*     } */
/*     else if (!strcmp(tagType, "stmt")){ */
/*         retVal.len += 13; // tag length */
/*         retVal.str = (char*)malloc((retVal.len+1)*sizeof(char)); */
/*         *(retVal.str) = '\0'; */
/*         strcat(retVal.str, "<stmt>"); */
/*         for (i = 0; i < numChild; i++) { */
/*             strcat(retVal.str, ndArr[i].str); */
/*             free(ndArr[i].str); */
/*         } */
/*         strcat(retVal.str, "</stmt>"); */
/*     } */
    
/*     return retVal; */
/* } */



